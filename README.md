# CHMI weather radar API research

## Timezone
* all versions use GMT

## URL format
* `PREFIX`.z_max3d.`YYYYMMDD`.`HHMM`.0.png
* for all versions except [Meteor](#version-used-by-the-meteor-android-app) and [radareu.cz](#version-used-by-radareucz-most-of-europe)

## Map projection
* AFAIK, all versions use EPSG:3857 (Google/Web Mercator)

## Versions
### Version used by the original [JSRadView](http://portal.chmi.cz/files/portal/docs/meteo/rad/data_jsradview.html)
* PREFIX = `http://portal.chmi.cz/files/portal/docs/meteo/rad/data_tr_png_1km/pacz23`
* 15 minute interval
* images saved for approximately 2.5 days
* w/ borders
* big copyright text
* 810x610

### Version used by the [new INCA viewer](http://portal.chmi.cz/files/portal/docs/meteo/rad/inca-cz/short.html)
* PREFIX = `http://portal.chmi.cz/files/portal/docs/meteo/rad/inca-cz/data/czrad-z_max3d/pacz2gmaps3`
* 10 minute interval
* w/ borders
* cropped JSRadView image
* big copyright text
* 680x460
* coordinates
  * top left - 52.167, 11.267
  * bottom right - 48.047, 20.77

### Version used by [radar.bourky.cz](http://radar.bourky.cz/)
* PREFIX = `http://radar.bourky.cz/data/pacz2gmaps`
* 10 minute interval
* w/o borders
* slightly different position compared to the 2 versions before
* small copyright text
* 728x528
* coordinates
  * top left - 51.889, 10.060
  * bottom right - 47.090, 20.223

### Version used by [radar4ctu.bourky.cz](http://radar4ctu.bourky.cz)
* PREFIX = `http://radar4ctu.bourky.cz/data/pacz2gmaps`
* same as the [radar.bourky.cz version](#version-used-by-radarbourkycz), except this version is without corrections and has a 15 minute interval

### Version used by the [Meteor Android app](https://play.google.com/store/apps/details?id=org.androworks.meteor)
* http://`1 or 2 or 3`.meteor.androworks.org/images/radar/image-`YYYYMMDD`.`HHMM` - images
* http://`1 or 2 or 3`.meteor.androworks.org/images/thumb/image-`YYYYMMDD`.`HHMM` - thumbnails
* http://`1 or 2 or 3`.meteor.androworks.org/feed - the `X-Frame-Date` HTTP header has the latest available frame in a `YYYYMMDD`.`HHMM` format
* 10 minute interval
* images saved for 5 hours (last 30 images)
* w/o borders
* no copyright text
* 597x377
* coordinates
  * top left - 51.452389, 11.289632
  * bottom right - 48.062307, 19.613042

### Version used by [radareu.cz](http://www.radareu.cz/) - most of Europe
* PREFIX = `http://www.radareu.cz/data/radar/radar.anim`
* FORMAT = `PREFIX`.`YYYYMMDD`.`HHMM`.0.png
* 15 minute interval
* images saved since `20160329.0430`
* w/o borders
* no copyright text
* 3196x4000
* coordinates
  * top left - 72.62025190354672, -14.618225054687514
  * bottom right - 30.968189526345665, 45.314636273437486
